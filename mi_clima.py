#!/usr/bin/env python3
#
# https://openweathermap.org/
# Documentación PyOWM -> https://pyowm.readthedocs.io/en/latest/

import pyowm

class mi_clima():
    def __init__( self, mi_token ):
        try:
            self.owm = pyowm.OWM( mi_token )
            print( 'mi_clima() => Conectado' )
        except:
            print( 'mi_clima() => Error al conectar' )

    def _descomponer( self, cadena ):
        if cadena.find( ',' )>0:
            pais = cadena[ cadena.find( ',' ) + 1 : len( cadena ) ].strip()
            pais = pais.upper()
            if len( pais ) > 2:
                pais = pais[ 0 : 1 ]
            elif len( pais ) < 2:
                pais = ''
            ciudad = cadena[ 0 : cadena.find( ',' ) ].strip()
        else:
            ciudad = cadena
            pais = ''

        return ciudad, pais

    def tiempo_ahora( self, ciudad_pais ):
        ciudad, pais = self._descomponer( ciudad_pais )

        reg = self.owm.city_id_registry()
        try:
            if pais != '':
                # ids = reg.ids_for( ciudad, country = pais, matching='like' )
                ids = reg.ids_for( ciudad, country = pais )
            else:
                if not( ciudad.isdigit() ):
                    # ids = reg.ids_for( ciudad, matching='like' )
                    ids = reg.ids_for( ciudad )
        except:
            ciudad = ''
            ciudad_id = 0

        if ciudad.isdigit():
            ciudad_id = int( ciudad )
            print( 'tiempo_ahora() => Has pasado el id ' + str( ciudad ) )
        elif len( ids ) <= 0:
            ciudad_id = 0
            texto = 'No existe ' + ciudad
            url_icono = ''
            print( 'tiempo_ahora() => ' + str( ciudad ) + ' no encontrada' )
        elif len( ids ) == 1:
            ciudad_id = ids[0][0]
            print( 'tiempo_ahora() => Sólo coincide una ciudad para ' + str( ciudad ) )
        elif len( ids ) > 1:
            # Primero comprobar si las coordenadas de todas las opciones son las mismas
            iguales = False
            for sitio in ids:
                coordenadas = reg.locations_for( sitio[1], country=sitio[2] )
                longitud = coordenadas[0].get_lon()
                latitud = coordenadas[0].get_lat()
                if not( iguales ):
                    lon_anterior = longitud
                    lat_anterior = latitud
                    iguales = True
                else:
                    if lon_anterior != longitud or lat_anterior != latitud:
                        iguales = False
                        break
                    else:
                        lon_anterior = longitud
                        lat_anterior = latitud

            if iguales:
                ciudad_id = int( ids[0][0] )
                print( 'tiempo_ahora() => Todas las coincicidencias para ' + str( ciudad ) + ' tienen las mismas coordenadas' )
            else:
                ciudad_id = 0
                texto = 'Indefinido (lon,lat):'
                
                sin_duplicados = []
                for sitio in ids:
                    str_sitio = str( sitio )
                    str_sitio = str_sitio.replace( '\'', '' )
                    str_sitio = str_sitio.replace( '(', '' )
                    str_sitio = str_sitio.replace( ')', '' )

                    coordenadas = reg.locations_for( sitio[1], country=sitio[2] )
                    str_longitud = str( round( coordenadas[0].get_lon(), 4 ) ).strip()
                    str_latitud = str( round( coordenadas[0].get_lat(), 4 ) ).strip()

                    mis_coor = { 'longitud':str_longitud, 'latitud':str_latitud }
                    if not( mis_coor in sin_duplicados ):
                        sin_duplicados.append( mis_coor )
                        texto = texto + '\n- ' + str_sitio + ' -> ' + str_latitud + ',' +  str_longitud

                texto = texto + '\nPuedes comprobar en https://www.openstreetmap.org/ con las  coordenadas.'
                url_icono = ''
                print( 'tiempo_ahora() => Hay varias opciones para ' + str( ciudad ) )

        if ciudad_id != 0:
            observacion = self.owm.weather_at_id( ciudad_id )
            w = observacion.get_weather()

            l = observacion.get_location()
            texto = 'Ciudad: ' + str( l.get_ID() )
            texto = texto + ', ' + l.get_name()
            texto = texto + ' (' + str( l.get_lat() ) + ', ' + str( l.get_lon() ) + ')'

            texto = texto + '\nEstado: ' + str( w.get_detailed_status() )
            texto = texto + '\n---'
            texto = texto + '\nTemperatura ' + str( w.get_temperature( unit='celsius' )['temp'] ) + '°C'
            texto = texto + '\nHumedad ' + str( w.get_humidity() ) + '%'
            texto = texto + '\nPresión ' + str( w.get_pressure()['press'] ) + 'mb'
            viento = round( w.get_wind()['speed'] * 3600 / 1000 )
            texto = texto + '\nViento ' + str( viento ) + 'km/h'
            texto = texto + '\nNubosidad: ' + str( w.get_clouds() ) + '%'
            texto = texto + '\nLluvia: ' + str( w.get_rain() )
            texto = texto + '\nNieve: ' + str( w.get_snow() )

            url_icono = w.get_weather_icon_url()
        
        print( 'Longitud de texto = ', len( texto ) )
        salida = { 'texto':texto, 'url_icono':url_icono }

        return salida
