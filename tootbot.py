#!/usr/bin/env python3
#
# toot-bot.py
# Librería para las funciones del bot genérico
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# LIBRERIAS
from frases import buscar_en_fich
from random import randint
import social

# CONSTANTES
PRENITTER = 'https://nitter.net/'
NUMTUITS = 10

# FUNCIONES
class Destinatario:
    def __init__( self ):
        self._destinatario = ''

    def buscar( self, texto ):
        salida = ''
        inicio = texto.find( '<a>' )
        if inicio > 0:
            inicio = inicio + 3
            cadena = texto[ inicio : len( texto ) ].strip()
            cadena = cadena[ 0 : len( cadena ) ]
            if cadena[ 0 ] == '<':
                fin = cadena.find( '>' )
                if fin > 1:
                    salida = cadena[ 1 : fin ]
        self._destinatario = salida

    def existe( self ):
        salida = False
        if self._destinatario != '':
            salida = True
        return salida

    def valor( self ):
        return( self._destinatario )

def lista_bots( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Listado de mis bots'
    mensaje = mensaje + '\n---\n'
    mensaje = mensaje + '@insultos@botsin.space             Bot de insultos\n'
    mensaje = mensaje + '@ChuckNorris_ESP@botsin.space      Bot con frases sobre Chuck Norris\n'
    mensaje = mensaje + '@Mariano_Rajoy_bot@botsin.space    Bot con frases de Mariano Rajoy\n'
    mensaje = mensaje + '@chistes_ES@botsin.space           Bot con chistes\n'
    mensaje = mensaje + '@RAE_no_oficial@botsin.space       Bot con la palabra del día y búsquedas en el DRAE\n'
    mensaje = mensaje + '@tootear@botsin.space              Yo mismo'
    mensaje = mensaje + '@amb_noticias@botsin.space         Bot de noticias'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def ayuda( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Bot de proceso de mensajes'
    mensaje = mensaje + '\n---\n'
    mensaje = mensaje + 'El bot procesa un comando dentro del mensaje con las siguientes opciones:\n'
    mensaje = mensaje + '<a> Añadir un destinatario a continuación entre <> y sin @ inicial\n'
    mensaje = mensaje + '<b> Listado de mis bots\n'
    mensaje = mensaje + '<h> Esta ayuda\n'
    mensaje = mensaje + '<i> Recibir un insulto\n'
    mensaje = mensaje + '<p> Recibir un piropo\n'
    mensaje = mensaje + '<t> Tootear lo que se encuentre a continuación del comando\n'
    mensaje = mensaje + '<y> Mensaje al azar de una cuenta de historia de Nitter\n'
    mensaje = mensaje + '<w> El tiempo en la localidad marcada a continuación\n'
    mensaje = mensaje + '\n\nNo se procesan solicitudes de bots\n'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def run_frases( masto, notificacion, fich_ins, destinatario ):
    solicitante = masto.solicitante_notif( notificacion )
    cabecera = ''
    if destinatario == '':
        cabecera = '@' + solicitante + '\n'
    else:
        cabecera = '@' + destinatario + ' , @' + solicitante + ' quiere decirte algo:\n'

    respuesta = buscar_en_fich( fich_ins, cabecera )
    respuesta = masto.citados( notificacion, respuesta )

    if len( respuesta ) >= masto.masto_max():
        respuesta = "Ha habido un error @" + solicitante + "\nPor favor, vuelve a intentarlo."

    masto.toot_texto( respuesta, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def enviar_mensaje( masto, notificacion , destinatario, id_mensaje ):
    cuerpo = masto.cuerpo_notif( notificacion )
    inicio = cuerpo.find( '<t>' ) + 3
    texto_mensaje = cuerpo[ inicio : len( cuerpo ) ]
    solicitante = masto.solicitante_notif( notificacion )
    if destinatario == '':
        texto_mensaje = '@' + solicitante + ' dice:\n' + texto_mensaje
    else:
        texto_mensaje = '@' + destinatario + ' , @' + solicitante + ' quiere decirte algo:\n' + texto_mensaje

    fotos = notificacion['status']['media_attachments']
    masto.toot_mensaje( texto_mensaje, True, fotos )

def mensaje_historia( masto, notificacion , destinatario, f_lista_hist ):
    solicitante = masto.solicitante_notif( notificacion )

    lista_cuentas = []
    f = open ( f_lista_hist, 'r' )
    lista_cuentas = f.readlines()
    f.close()
    n_cuenta = randint( 0, len( lista_cuentas ) - 1 )
    cuenta = PRENITTER + lista_cuentas[n_cuenta].strip()
    print( 'mensaje_historia => cuenta = ', cuenta )

    tuits = social.getTweets( cuenta, 'hist2mast', NUMTUITS )
    if tuits:
        num_tuits = len( tuits )
        if num_tuits > 0:
            n_tuit = randint( 0, num_tuits - 1 )
            tuit = tuits[n_tuit]
    
        if destinatario == '':
            cabecera = '@' + solicitante + '\n'
        else:
            cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'

        social.toot_sc( tuit, masto, notificacion, cabecera )
    else:
        respuesta = '@' + solicitante + ' Ha habido un ERROR; vuelve a intenarlo'
        masto.toot_texto( respuesta, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def mensaje_clima( masto, notificacion , destinatario, el_clima ):
    cuerpo = masto.cuerpo_notif( notificacion )
    inicio = cuerpo.find( '<w>' ) + 3
    ciudad = cuerpo[ inicio : len( cuerpo ) ]
    ciudad = ciudad.strip()

    texto = el_clima.tiempo_ahora( ciudad )['texto']
    
    solicitante = masto.solicitante_notif( notificacion )
    if destinatario == '':
        cabecera = '@' + solicitante + '\n'
    else:
        cabecera = '@' + destinatario + ' , @' + solicitante + ' te envía:\n'
    texto = cabecera + texto

    masto.toot_texto( texto, masto.id_notif( notificacion), masto.visibilidad_notif( notificacion ) )

def comandos( masto, notificacion, nf_insultos, nf_piropos, el_clima, f_lista_hist, id_mensaje = '' ):
    # Comandos: a, h, i, p, t, r, w
    minus = masto.cuerpo_notif( notificacion ).lower()

    destinatario = ''
    if minus.find( '<a>' )>0:
        print( 'tootbot - comandos => TEXTO = ', minus )
        dest = Destinatario()
        dest.buscar( minus )
        destinatario = dest.valor()
        print( 'tootbot - comandos => Comando <a> --- Destinatario = ', destinatario )

    if minus.find( '<b>' )>0:
        print( 'tootbot - comandos => Comando <b>' )
        lista_bots( masto, notificacion )
    elif minus.find( '<h>' )>0:
        print( 'tootbot - comandos => Comando <h>' )
        ayuda( masto, notificacion )
    elif minus.find( '<i>' )>0:
        print( 'tootbot - comandos => Comando <i>' )
        run_frases( masto, notificacion, nf_insultos, destinatario )
    elif minus.find( '<p>' )>0:
        print( 'tootbot - comandos => Comando <p>' )
        run_frases( masto, notificacion, nf_piropos, destinatario )
    elif minus.find( '<t>' )>0:
        print( 'tootbot - comandos => Comando <t>' )
        enviar_mensaje( masto, notificacion, destinatario, id_mensaje )
    elif minus.find( '<y>' )>0:
        print( 'tootbot - comandos => Comando <y>' )
        mensaje_historia( masto, notificacion, destinatario, f_lista_hist )
    elif minus.find( '<w>' )>0:
        print( 'tootbot - comandos => Comando <w>' )
        mensaje_clima( masto, notificacion, destinatario, el_clima )
    else:
        print( 'tootbot - comandos => No hay comandos' )
    return
