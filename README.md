# Mega bot de Mastodon

___15/04/2020___
Intento de correr todos mis bots en un solo proceso para poderlos tener gratuítamente en Heroku.

De momento tengo esto:

	1. Bot para Mastodon que lee un fichero de texto con frases o palabras y ejecuta dos tareas (basado en la idea de Cheap Bots, Toot Sweet! <https://cheapbotstootsweet.com>):

		+ Publicar periódicamente una frase o palabra
		+ Responder con una frase o palabra cuando se le cita (comando <r>)
		+ Ayuda (comando <h>)
	
	Bot original: https://gitlab.com/ambrosthegreat/frases2mastodon

	2. Bot que publica en Mastodon un chiste extraído de https://www.chister.com

		+ Publicar periódicamente un chiste
                + Responder con un chiste cuando se le cita (comando <r>)
                + Ayuda (comando <h>)

	Bot original: https://gitlab.com/ambrosthegreat/chistes

Puedes seguir los bots en estas cuenta:

	https://botsin.space/@insultos
	https://botsin.space/@Mariano_Rajoy_bot
	https://botsin.space/@ChuckNorris_ESP
	https://botsin.space/@chistes_ES

Desarrollado por https://hispatodon.club/@AmbrosTheGreat

___16/04/2020___
Añadido tootbot con las siguientes funciones:
	
	+ Listado de mis bots
	+ Recibir un insulto
	+ Recibir un piropo
	+ Tootear lo que se encuentre a continuación del comando
	+ Se puede hacer que las respuestas del bot vayan a un destinatario determinado

___17/04/2020___
Incorporado bot de la RAE:

	+ Palabra del día
	+ Búsqueda de palabras en el DRAE
	+ Tootear lo que tuitea la RAE

___18/04/2020___
Incorporado a tootbot:

	+ Comando <y> que tootea un tuit de historia

___23/04/2020___
Incorporado bot de noticias disponible en https://botsin.space/@amb_noticias

___14/06/2020___
Saco el bot de noticas para que funcione por separado con el Heroku Scheduler
Código en: https://gitlab.com/ambrosthegreat/feeds2mastodon
