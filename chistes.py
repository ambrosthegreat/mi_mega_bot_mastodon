#!/usr/bin/env python3
#
# chistes.py
#
# Librería para extraer un chiste de una web y poder tootearlo
#
""" Candidatos para tomar el chiste:
            http://www.chistes.com/chistealazar.asp?n=3
            https://www.chister.com/azar.php --- YA NO FUNCIONA
            http://www.chistescalientes.com/ChisteAlAzar.asp?n=3
            https://elclubdeloschistes.com/azar.php
            https://chistalia.es/aleatorio/
            http://www.todo-chistes.com/chistes-al-azar

        Fuente para capturar cuando se cita en Mastodon copiado de:
            https://www.bortzmeyer.org/fediverse-bot.html
            https://framagit.org/bortzmeyer/mastodon-DNS-bot
"""

# CONSTANTES
# URL_CHISTES = 'https://www.chister.com/azar.php'
# La página anterior ha dejado de dar servicio
URL1 = 'http://www.chistes.com/chistealazar.asp?n=3'
URL2 = 'https://elclubdeloschistes.com/azar.php'
URL3 = 'https://chistalia.es/aleatorio/'
URL4 = 'http://www.todo-chistes.com/chistes-al-azar'
URL5 = 'http://www.chistescalientes.com/ChisteAlAzar.asp?n=3'
NUM_URL = 5
INTENTOS = 5 # Numero de intentos para conseguir chiste; si no se localiza no se publica nada
TIEMPO_ESPERA = 1
MASTODON_MAX = 450

# LIBRERIAS
from bs4 import BeautifulSoup
from datetime import datetime
from dateutil import tz
from random import randint
import requests
import time

# FUNCIONES
def _abrir_pagina( la_URL ):
    # Abrir página y meter en un objeto
    pagina_web = requests.get( la_URL )
    bsObj = BeautifulSoup( pagina_web.content, 'lxml' )
    return bsObj

def _buscar_pagina( bsObj ):
    # Buscar chiste en la  pagina
    salida = bsObj.find( 'div', {'class': 'chiste'})
    return salida.get_text()

def _cab_chistaco():
    zona = tz.gettz( 'Europe/Madrid' )
    hora_local = datetime.now()
    hora = hora_local.astimezone( zona )
    salida = 'Chistaco de las ' + hora.strftime("%X") + '\n---\n'
    return salida

def _busca_URL():
    lista_URL = []
    lista.append( URL1 )
    lista.append( URL2 )
    
    id = randint( 1, len( lista_URL ) - 1 )
    return lista_URL[ id ]

def _extraer_URL1():
    # Buscar chiste en la pagina chistes.com
    bsObj = _abrir_pagina( URL1 )
    salida = bsObj.find( 'div', {'class': 'chiste'}).get_text()
    salida = salida.strip()
    
    if salida != '':
        salida = salida + '\n\nFuente: chistes.com'
    return salida

def _extraer_URL2():
    # Buscar chiste en la pagina elclubdeloschistes.com
    bsObj = _abrir_pagina( URL2 )
    salida = bsObj.find( 'tr', {'class': 'texto'} )

    salida = str( salida )
    salida = salida[ salida.find( '</b>' ) + len( '</b>' ) : salida.find( '<br/>   ID:' ) ]
    salida = salida.replace( '<br/>', '' )
    salida = salida.replace( '<br>', '\n' )
    salida = salida.strip()

    if salida !='':
        salida = salida + '\n\nFuente: elclubdeloschistes.com'
    return salida

def _extraer_URL3():
    # Buscar chiste en la pagina chistalia.es
    bsObj = _abrir_pagina( URL3 )
    salida = bsObj.find( 'div', {'class': 'content-entry'} )
    
    salida = str( salida)
    if salida.find( '<blockquote><br/>' ) > 0:
        salida = salida[ salida.find( '<blockquote><br/>' ) + len( '<blockquote><br/>' ) : salida.find( '<br/></blockquote>' ) ]
        salida = salida.replace( '<br>', '\n' )
        salida = salida.replace( '<br/>', '\n' )
        salida = salida.strip()
        if salida.find( '<' ) > 0:
            salida = salida[ 0: salida.find( '<' ) ]
        salida = salida + '\n\nFuente: chistalia.es'
    else:
        salida = ''

    return salida

def _extraer_URL4():
    # Buscar chiste en la pagina todo-chistes.com
    bsObj = _abrir_pagina( URL4 )
    salida = bsObj.find( 'div', {'class': 'field-chiste'}).get_text()
    salida = salida.strip()

    if salida != '':
        salida = salida + '\n\nFuente: todo-chistes.com'
    return salida

def _extraer_URL5():
    # Buscar chiste en la pagina chistescalientes.com
    bsObj = _abrir_pagina( URL1 )
    salida = bsObj.find( 'div', {'class': 'chiste'}).get_text()
    salida = salida.strip()

    if salida != '':
        salida = salida + '\n\nFuente: chistescalientes.com'
    return salida

def buscar_chiste( a_anadir = '' ):
    # Buscar chiste
    if a_anadir == '':
        cabecera = _cab_chistaco()
    else:
        cabecera = a_anadir

    contador = INTENTOS
    chiste = ''
    while contador > 0 and ( len( chiste )<1 or len( chiste ) > MASTODON_MAX ):
        time.sleep( TIEMPO_ESPERA )
        id_url = randint( 1, NUM_URL )
        if id_url == 1:
            chiste = _extraer_URL1()
        elif id_url == 2:
            chiste = _extraer_URL2()
        elif id_url == 3:
            chiste = _extraer_URL3()
        elif id_url == 4:
            chiste = _extraer_URL4()

        contador += 1

    if len( chiste ) > 0:
        chiste = cabecera + str( chiste )

    return chiste
