#!/usr/bin/env python3
#
# busca_palabra
# Librería para buscar un palabra en el diccionario de la RAE
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# LIBRERIAS
from bs4 import BeautifulSoup
from lxml import html
import requests

# CONSTANTES
MASTODON_MAX = 500

# LIBRERIAS
def _ayuda( masto, notificacion ):
    mensaje = '@' + masto.solicitante_notif( notificacion ) +'\n'
    mensaje = mensaje + 'Bot de proceso de mensajes\n---\n'
    mensaje = mensaje + 'El bot procesa un comando dentro del mensaje con las siguientes opciones:\n'
    mensaje = mensaje + '<h> Esta ayuda\n'
    mensaje = mensaje + '<r> Devolver una definición del DRAE\n'
    mensaje = mensaje + '\nNo se procesan solicitudes de bots\n'
    masto.toot_texto( mensaje, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def _extraer_palabra_mensaje( masto, notificacion ):
    mensaje = masto.cuerpo_notif( notificacion )

    palabra = mensaje[ mensaje.find( '<r>' ) + 3 : len( mensaje ) ]

    palabra = palabra.strip() + " "
    palabra = palabra[ 0 : palabra.find( " " ) ]
    palabra = palabra.strip()

    return palabra

def _extraer_cad_definicion( la_URL ):
    # Abrir página, buscar palabra dia y devolver la cadena que la contiene
    bsObj = BeautifulSoup( requests.get( la_URL ).content, 'lxml' )
    cadena = str( bsObj.find( id='resultados' ) )
    doc = html.document_fromstring( cadena )
    cadena = doc.text_content()
    cadena = cadena[ 0 : cadena.find( "©" )-len( "Real Academia Española " ) ]
    cadena = cadena.strip()
    return cadena

def buscar_en_drae( palabra ):
    # Buscar una palabra en el diccionario
    enlace = "https://dle.rae.es/" + palabra + "?m=form"
    definicion = _extraer_cad_definicion( enlace )
    definicion = definicion[ 0 : definicion.find( chr( 10 ) ) ] + chr( 10 ) + definicion[ definicion.find( chr( 10 ) ) : len( definicion ) ]
    return definicion, enlace

def _tratar_cuerpo( masto, notificacion ):
    palabra = _extraer_palabra_mensaje( masto, notificacion )

    if len( palabra ) == 0:
        salida = '--- Ha habido un error en tu consulta ---'
    else:
        cabecera = '@' + masto.solicitante_notif( notificacion ) + '\n---\n'
        citados = masto.citados( notificacion )
        definicion, enlace = buscar_en_drae( palabra )
        cola = '\n\nEnlace: ' + enlace
        if citados !='':
            cola = cola + '\n' + citados

        long_max = MASTODON_MAX - len( cabecera ) - len( definicion) - len( cola )
        if len( definicion ) > long_max:
            cad_union = ' [...]'
            long_max = long_max - len( cad_union )
            definicion = definicion[ 0 : long_max ] + ' [...]'

        salida = cabecera + definicion + cola

    return salida

def run_def( masto, notificacion ):
    respuesta = _tratar_cuerpo( masto, notificacion )
    masto.toot_texto( respuesta, masto.id_notif( notificacion ), masto.visibilidad_notif( notificacion ) )

def comandos( masto, notificacion ):
    # Comandos: h, r
    minus = masto.cuerpo_notif( notificacion ).lower()
    if minus.find( '<h>' )>0:
        _ayuda( masto, notificacion )
    elif minus.find( '<r>' )>0:
        run_def( masto, notificacion )
    return
