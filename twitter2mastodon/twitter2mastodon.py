#!/usr/bin/env python3
from argparse import ArgumentParser
import encodings.idna
import social
import sys

def args():
    #Argumentos
    parser = ArgumentParser( description='Importar a Mastodon una cuenta de Twitter' )
    parser.add_argument( '-a', '--app_token', help='Token de la app registrada en la cuenta a usar', type=str, required = True )
    parser.add_argument( '-i', '--inst_url', help='URL de la instancia a conectar', type=str, required = True )
    parser.add_argument( '-m', '--max_tuits', help='Náximo número de tuits a importar', type=int, required = True )
    parser.add_argument( '-n', '--nombre_app', help='Nombre de la app con la que se tootea', type=str, required = True )
    parser.add_argument( '-p', '--img_tmp', help='Fichero para guardar las imágenes temporalmente', type=str, required = True )
    parser.add_argument( '-t', '--temporal', help='Fichero para datos temporales', type=str, required = True )
    parser.add_argument( '-u', '--cuenta_url', help='URL de la cuenta a tootear', type=str, required = True )
    return parser.parse_args()

argumentos = args()
app_token = argumentos.app_token
app_url = argumentos.inst_url
tuit_url = argumentos.cuenta_url
mi_app_nombre = argumentos.nombre_app
img_tmp = argumentos.img_tmp
timestamp_file = argumentos.temporal
max_tuits = argumentos.max_tuits

if __name__ == "__main__":

    """ It all starts here...

    This function will get a new Tweet from the configured Twitter account and publish to the configured Mastodon instance.
    It will only toot once per invokation to avoid flooding the instance.
    """

    tweets = social.getTweets( tuit_url, mi_app_nombre, max_tuits )

    if not tweets:
        print("__main__ => No tweets fetched.")
        sys.exit()

    print("__main__ => {len(tweets)} tweets fetched.")

    for tweet in tweets:
        if social.tootTheTweet(tweet, app_url, app_token, timestamp_file, img_tmp ):
            print('__main__ => Tooted "{tweet["text"]}"')
            print("__main__ => Tooting less is tooting more. Sleeping...")
            sys.exit()
