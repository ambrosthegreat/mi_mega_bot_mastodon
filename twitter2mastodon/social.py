#!/usr/bin/env python3
# Gran trabajo de Ayush Sharma: https://github.com/ayush-sharma/tweet-toot
# Sin embargo, no había manera de importar los tuits, creo que Twitter ha cambiado algo y su método se ha quedado obsoleto
# Así que he tenido que rehacerlo casi entero aunque basámdoe en su trabajo
# Documentación de Mastodon: https://mastodonpy.readthedocs.io/en/stable/#

from bs4 import BeautifulSoup
from datetime import datetime
import mi_dir
import helpers
import magic
import requests
import time

TIEMPO_ESPERA = 5

def getTweets( mi_url, app_name, max_elem ):
    """ Get list of tweets, with tweet ID and content, from configured Twitter account URL.
    This function relies on BeautifulSoup to extract the tweet IDs and content of all tweets on the specified page.
    The data is returned as a list of dictionaries that can be used by other functions.
    """

    all_tweets = []
    print( 'getTweets() => Arrancando.......' )

    if not mi_url:
        print("getTweets() => The source Twitter account URL ({url}) was incorrect. Could not retrieve tweets.")
        return False

    headers = {}
    headers["accept-language"] = "es-ES,es;q=0.9, en;q=0.8, fr-CH,fr;q=0.9, *;q=0.5"
    headers["dnt"] = "1"
    headers["user-agent"] = app_name

    # data = requests.get( mi_url ).content
    data = requests.get( mi_url ).text
    html = BeautifulSoup( data, "html.parser" )
    # Cambiar referencias a nitter y texto de los enlaces a la propia referencia
    for c_enlace in html.findAll( 'a' ):
        if c_enlace.get( 'href' ).find( 'http' ) < 0:
            c_enlace['href'] = 'https://nitter.net' + c_enlace['href']
            c_enlace.string = str( c_enlace.get( 'href' ) )
    for c_enlace in html.findAll( 'img' ):
        c_enlace['src'] = 'https://nitter.net' + c_enlace['src']

    contador = 1

    # Cada tweet es un elemento timeline-item
    elementos = html.find_all( 'div' , {'class': 'timeline-item'} )
    for elemento in elementos:
        try:
            # Enlace al tuit
            enlace = elemento.find( 'a', {'class': 'tweet-link'}, href=True )
            # Cuerpo del tuit
            cuerpo = ''
            cuerpo_html = elemento.find( 'div' , {'class': 'tweet-content media-body'} )
            cuerpo = cuerpo_html.get_text()
            cuerpo = cuerpo.replace( 'ow.ly', 'http://ow.ly' )
            # Fecha del tuit
            linea_fecha = elemento.find( 'span', {'class': 'tweet-date'} )
            subfecha = linea_fecha.find( 'a' )
            str_fecha = str( subfecha.get( 'title' ) )
            fecha = datetime.strptime( str_fecha, '%d/%m/%Y, %H:%M:%S' )
            # Id del tuit
            # ENLACE = <a class="tweet-link" href="https://nitter.net/RAEinforma/status/1247132017105473536#m">
            tuit_id = str( enlace.string[ enlace.string.find( '/status/' ) + len ( '/status/' ) : len( enlace.string ) ] )
            tuit_id = tuit_id.strip()
            # Lista de imágenes
            lista_imagenes = []
            imagenes = elemento.findAll( 'a', {'class': 'still-image'} )
            for imagen in imagenes:
                cadena = imagen.get( 'href' )
                cadena = cadena.replace( '/pic/http', 'http' )
                cadena = cadena.strip()
                cadena = cadena[ 0 : cadena.find( '.jpg' ) + 4 ]
                cadena = cadena.replace( '%3A', ':' )
                cadena = cadena.replace( '%2F', '/' )
                if cadena[0:4] == 'http' and cadena[len(cadena)-4:len(cadena)] == '.jpg':
                    lista_imagenes.append( cadena )

            # Diccionario con los datos del tuit
            all_tweets.append( { "id": tuit_id, "cuerpo": cuerpo, "url": enlace, "time": str( fecha ), "imagenes": lista_imagenes } )

            contador += 1
            if contador > max_elem:
                break

        except Exception as e:
            print( "getTweets() => No tweet text found." )
            print( e )
            continue

    print( 'getTweets() => Terminado.' )
    return all_tweets if len(all_tweets) > 0 else None

def subir_tuit_media( mastodon, foto ):
    img_local = requests.get( foto ).content
    mi_magic = magic.Magic( mime = True )
    tipo_mime = mi_magic.from_buffer( img_local )
    print( 'toot_mensaje() => TIPO MIME = ', tipo_mime )
    foto_subida = mastodon.media_post( img_local, mime_type = tipo_mime )
    return foto_subida

def tootTheTweet( tweet, masto, timestamp_file, ocultar_foto = False ):
    """ Receieve a dictionary containing Tweet ID and text... and TOOT!
    This function relies on the requests library to post the content to your Mastodon account (human or bot).
    A boolean success status is returned.
    Arguments:
        tweet {dictionary} -- Dictionary containing the "id" and "text" of a single tweet.
    """""

    print( 'tootTheTweet() => Arrancando.......' )
    
    last_timestamp = helpers._read_file(timestamp_file)
    if not last_timestamp:
        helpers._write_file(timestamp_file, str(tweet['time']))
        return False

    print( 'tootTheTweet ===> ', tweet["cuerpo"], '\nTIME = ', tweet['time'], '   last_timestamp = ', last_timestamp )
    if tweet['time'] <= last_timestamp:
        print('tootTheTweet() => No new tweets. Moving on.')
        return None

    last_timestamp = helpers._write_file(timestamp_file, str(tweet['time']))

    print( 'tootTheTweet() => New tweet {', tweet["id"], '} => "{', tweet["cuerpo"], '}".')

    if tweet["cuerpo"]:
        cuerpo = tweet["cuerpo"] + '\n\nFuente: ' + str( tweet["url"].get( 'href' ) )

    lista_fotos = []
    for imagen in tweet["imagenes"]:
        nueva_foto = subir_tuit_media( masto.mastodon, imagen )
        lista_fotos.append( nueva_foto )
        
    masto.toot_mensaje( cuerpo, ocultar_foto, lista_fotos, id_mensaje = tweet["id"] )
    print( 'tootTheTweet() => Terminado.' )

def toot_sc( tweet, masto, notificacion, cabecera = '' ):
    if tweet['cuerpo']:
        cuerpo = tweet['cuerpo'] + '\n\nFuente: ' + str( tweet['url'].get( 'href' ) )

    cuerpo = cabecera + cuerpo

    lista_fotos = []
    for imagen in tweet['imagenes']:
        nueva_foto = subir_tuit_media( masto.mastodon, imagen )
        lista_fotos.append( nueva_foto )

    masto.mastodon.status_post( cuerpo, in_reply_to_id = masto.id_notif( notificacion), media_ids = lista_fotos, visibility = masto.visibilidad_notif( notificacion ) )
    print( 'toot_sc() => Nuevo tuit {' + cuerpo + '}' )
