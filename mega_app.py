#!/usr/bin/env python3
#
# mega_app
# Función 1: Ejecutar proceso de 
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
T_ESPERA = 5        # Tiempo de espera entre comprobación de eventos
T_RECUPERA = 30     # Tiempo de recuperación en segundos después de un error
TA_INSULTOS = 1     # Horas hasta siguiente frase de insultos
TA_RAJOY = 6        # Horas hasta siguiente frase de Rajoy
TA_CHUCK = 3        # Horas hasta siguiente frase de Chuck Norris
TA_CHISTES = 1      # Horas hasta siguiente chiste
MAX_TUITS = 10      # Máximo número de tuits a importar por ejecución
URL_RAE = 'https://nitter.net/@RAEinforma'
URL_PARDINO = 'https://nitter.net/@ProfedonPardino'
URL_ASALE = 'https://nitter.net/@ASALEinforma'
HORA_RAE = 7        # Hora a la que buscar la palabra del día
MINUTOS_RAE = 15    # Minutos a los que buscar la palabra del día
TA_TUITS_RAE = 1    # Horas hasta siguiente actualización de la TL de la RAE
TA_PARDINO = 30     # Desfase en minutos entre los tuits de la RAE y de Pardino
TA_ASALE = 15
NUM_TUITS = 10      # Máximo número de tuits a importar de vez
MAX_NOTICIAS = 3    # Máximo número de noticias de cada medio a recoger cada cosulta

# LIBRERIAS
from argparse import ArgumentParser
from datetime import datetime, timedelta
from dateutil import tz
from palabra_del_dia import palabra_del_dia
from mi_clima import mi_clima
from mi_tiempo import mi_hora, toca_alarma
from random import randint
import busca_palabra
import frases
import mi_masto
import os
import social
import sys
import time
import tootbot

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica frases de un fichero de texto' )
    parser.add_argument( '-ab', '--app_token_chistes', help='Token de la app para chistes', type=str )
    parser.add_argument( '-ac', '--app_token_chuck', help='Token de la app para Chuck Norris', type=str )
    parser.add_argument( '-ad', '--app_token_rae', help='Token de la app para RAE', type=str )
    parser.add_argument( '-ai', '--app_token_insultos', help='Token de la app para insultos', type=str )
    parser.add_argument( '-ar', '--app_token_rajoy', help='Token de la app para Rajoy', type=str )
    parser.add_argument( '-at', '--app_token_tootbot', help='Token de la app para el automatizador', type=str )
    parser.add_argument( '-i', '--inst_url', help='URL de la instancia a conectar', type=str )
    parser.add_argument( '-r', '--raiz_path', help='Dónde almacentar los temporales', type=str )
    parser.add_argument( '-w', '--weather_token', help='Token de OpenWeatherMap', type=str )
    return parser.parse_args()

def arregla_path( ruta ):
    if ruta[len( ruta ) - 1 : len( ruta )] != '/':
                    ruta = ruta + '/'

def _trata_alarma( mastodon, alarma, horas, nom_fich='' ):
    salida = alarma

    if toca_alarma( alarma ):
        mi_frase = frases.buscar_frase( nom_fich )
        if mi_frase != '':
            mastodon.toot_texto( frases.buscar_frase( nom_fich ) )
            salida = alarma + timedelta( hours = horas )
    return salida

def _trata_notifs( mastodon, t_espera, nom_fich='' ):
    notifs = mastodon.comprueba_notif( t_espera )
    for notif in notifs:
        frases.comandos( mastodon, notif, nom_fich )

def _tratar( mastodon, alarma, horas, t_espera, nom_fich='' ):
    salida = _trata_alarma( mastodon, alarma, horas, nom_fich ) 
    _trata_notifs( mastodon, t_espera, nom_fich )
    return salida

def _trata_notifs_tootbot( mastodon, t_espera, nf_insultos, nf_piropos, el_clima, f_hist ):
    notifs = mastodon.comprueba_notif( t_espera )
    for notif in notifs:
        tootbot.comandos( mastodon, notif, nf_insultos, nf_piropos, el_clima, f_hist )

def _trata_alar_fija_rae( mastodon, alarma ):
    salida = alarma
    
    if toca_alarma( alarma ):
        mastodon.toot_texto( palabra_del_dia() )
        salida = alarma + timedelta( days = 1 )
    return salida

def _trata_notifs_rae( mastodon, t_espera ):
    notifs = mastodon.comprueba_notif( t_espera )
    for notif in notifs:
        busca_palabra.comandos( mastodon, notif )

def _trata_alar_tuits( mastodon, alarma, horas, url_twitter, num_tuits, tmp_file ):
    salida = alarma
    
    if toca_alarma( alarma ):
        # TOOTEAR ALGO
        tuits = social.getTweets( url_twitter, 'tuit2toot', num_tuits )
        if tuits:
            for tuit in tuits:
                # print( '_trata_alar_tuits ===> ', tuit["cuerpo"] )
                social.tootTheTweet( tuit, mastodon, tmp_file )

        salida = alarma + timedelta( hours = horas )

    return salida

# MAIN
args = _args()
hora = mi_hora()

m_insultos = mi_masto.Mi_masto( args.inst_url, args.app_token_insultos, args.raiz_path )
f_insultos = os.getcwd() + '/ficheros_frases/insultos.txt'
a_insultos = hora + timedelta( hours = TA_INSULTOS )

m_chuck = mi_masto.Mi_masto( args.inst_url, args.app_token_chuck, args.raiz_path )
f_chuck = os.getcwd() + '/ficheros_frases/chuck_norris.txt'
a_chuck = hora + timedelta( hours = TA_CHUCK )

m_rajoy = mi_masto.Mi_masto( args.inst_url, args.app_token_rajoy, args.raiz_path )
f_rajoy = os.getcwd() + '/ficheros_frases/rajoy.txt'
a_rajoy = hora + timedelta( hours = TA_RAJOY )

m_chistes = mi_masto.Mi_masto( args.inst_url, args.app_token_chistes, args.raiz_path )
# a_chistes = hora + timedelta( hours = TA_CHISTES )
a_chistes = hora + timedelta( seconds = 3 )

m_tootbot = mi_masto.Mi_masto( args.inst_url, args.app_token_tootbot, args.raiz_path )
f_piropos = os.getcwd() + '/ficheros_frases/piropos.txt'
f_hist = os.getcwd() + '/ficheros_frases/twitter_historia.nfo'
el_clima = mi_clima( args.weather_token )

m_rae = mi_masto.Mi_masto( args.inst_url, args.app_token_rae, args.raiz_path )
a_palabra = hora.replace( hour = HORA_RAE, minute = MINUTOS_RAE, second = 0, microsecond = 0 ) + timedelta( days = 1 )
a_tuit_rae = hora + timedelta( hours = TA_TUITS_RAE )
a_tuit_pardino = a_tuit_rae + timedelta( minutes = TA_PARDINO )
a_tuit_asale = a_tuit_rae + timedelta( minutes = TA_ASALE )
tmp_rae = os.getcwd() + args.raiz_path + 'timestamp_rae.tmp'
tmp_pardino = os.getcwd() + args.raiz_path + 'timestamp_pardino.tmp'
tmp_asale = os.getcwd() + args.raiz_path + 'timestamp_asale.tmp'

while True:
        #try:
        a_insultos = _tratar( m_insultos, a_insultos, TA_INSULTOS, T_ESPERA, nom_fich=f_insultos )
        a_rajoy = _tratar( m_rajoy, a_rajoy, TA_RAJOY, T_ESPERA, nom_fich=f_rajoy )
        a_chuck = _tratar( m_chuck, a_chuck, TA_CHUCK, T_ESPERA, nom_fich=f_chuck )

        a_chistes = _tratar( m_chistes, a_chistes, TA_CHISTES, T_ESPERA )

        _trata_notifs_tootbot( m_tootbot, T_ESPERA, f_insultos, f_piropos, el_clima, f_hist )

        a_palabra = _trata_alar_fija_rae( m_rae, a_palabra )
        _trata_notifs_rae( m_rae, T_ESPERA )
        a_tuit_rae = _trata_alar_tuits( m_rae, a_tuit_rae, TA_TUITS_RAE, URL_RAE, NUM_TUITS, tmp_rae )
        a_tuit_pardino = _trata_alar_tuits( m_rae, a_tuit_pardino, TA_TUITS_RAE, URL_PARDINO, NUM_TUITS, tmp_pardino )
        a_tuit_asale = _trata_alar_tuits( m_rae, a_tuit_asale, TA_ASALE, URL_ASALE, NUM_TUITS, tmp_asale )

        time.sleep( T_ESPERA )
        #except:
        #print( "main ERROR:", sys.exc_info()[0] )
        #time.sleep( T_RECUPERA )
